#include <iostream>
#include <stdio.h>
using namespace std;

class CustomerCounter {
private:
  int maximum_customers;
  int customer_count;
  
public:
  CustomerCounter(){
    maximum_customers = 12;
    customer_count = 0;
}
  
  void add(int customer_curr_num){  
    if((customer_count + customer_curr_num) < maximum_customers){
      customer_count = customer_count + customer_curr_num;
    }else{
      cout << "Customer count exceeded the max limit";
    }
}
  
  void subtract(int customer_curr_num){
    if(customer_count<=0){
      cout<< "Empty";
    }else{
       customer_count = customer_count - customer_curr_num;      
    }
}
  
  int getCount(){
    return customer_count;
}
};  

int main(){
  CustomerCounter cc;
  cc.add(5);
  cout << "\nPresent Count : " << cc.getCount() << "\n";
  cc.subtract(4);
  cout << "\n\nPresent Count : " << cc.getCount() << "\n";
  cc.add(20);
  cout << "\n\nPresent Count : " << cc.getCount() << "\n";
  cc.subtract(25);
  cout << "\n\nPresent Count : " << cc.getCount() << "\n";
}  
