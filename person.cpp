#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

class Person{
    string name;
    vector<Person*>friends;
  
    public:
    Person(string);
    void befriend(Person*);
    void unfriend(Person*);
    void print();
};

Person::Person(string name) : name(name), friends(vector<Person*>()){};

void Person::befriend(Person* person){
  this -> friends.push_back(person);
}

void Person::unfriend(Person* person){
  auto e = find(this -> friends.begin(), this -> friends.end(), person);
    if (e != this -> friends.end()) {
        this -> friends.erase(e);
    }
}

void Person::print() {
    cout << "\nName: " << this->name << "\n Friends ";

    for (auto a : this->friends) {
        cout << a -> name << " ";
    }

    cout << "\n" << endl;
}

int main(){
    Person Syed = Person("Syed");
    Person Rohit = Person("Rohit");
    Person Jaden = Person("Jaden");

    cout << "[People] \n";
    Syed.print();
    Rohit.print();
    Jaden.print();

    Syed.befriend(&Rohit);
    Syed.befriend(&Jaden);
    Rohit.befriend(&Jaden);
    Jaden.befriend(&Rohit);

    cout << "[Befriended] \n";
    Syed.print();
    Rohit.print();
    Jaden.print();

    cout << "[Unfriend Rohit from Syed] \n";
    Syed.unfriend(&Rohit);
    Syed.print();
}
